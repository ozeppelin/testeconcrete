//
//  RequestsBaseClass.swift
//
//  Created by Anderson Silva on 08/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class RequestsBaseClass: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let state = "state"
    static let requestedReviewers = "requested_reviewers"
    static let body = "body"
    static let links = "_links"
    static let locked = "locked"
    static let diffUrl = "diff_url"
    static let patchUrl = "patch_url"
    static let assignees = "assignees"
    static let milestone = "milestone"
    static let statusesUrl = "statuses_url"
    static let id = "id"
    static let reviewCommentUrl = "review_comment_url"
    static let base = "base"
    static let title = "title"
    static let commentsUrl = "comments_url"
    static let url = "url"
    static let issueUrl = "issue_url"
    static let user = "user"
    static let updatedAt = "updated_at"
    static let htmlUrl = "html_url"
    static let mergeCommitSha = "merge_commit_sha"
    static let number = "number"
    static let authorAssociation = "author_association"
    static let head = "head"
    static let commitsUrl = "commits_url"
    static let createdAt = "created_at"
    static let reviewCommentsUrl = "review_comments_url"
  }

  // MARK: Properties
  public var state: String?
  public var requestedReviewers: [Any]?
  public var body: String?
  public var links: RequestsLinks?
  public var locked: Bool? = false
  public var diffUrl: String?
  public var patchUrl: String?
  public var assignees: [Any]?
  public var milestone: RequestsMilestone?
  public var statusesUrl: String?
  public var id: Int?
  public var reviewCommentUrl: String?
  public var base: RequestsBase?
  public var title: String?
  public var commentsUrl: String?
  public var url: String?
  public var issueUrl: String?
  public var user: RequestsUser?
  public var updatedAt: String?
  public var htmlUrl: String?
  public var mergeCommitSha: String?
  public var number: Int?
  public var authorAssociation: String?
  public var head: RequestsHead?
  public var commitsUrl: String?
  public var createdAt: String?
  public var reviewCommentsUrl: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    state <- map[SerializationKeys.state]
    requestedReviewers <- map[SerializationKeys.requestedReviewers]
    body <- map[SerializationKeys.body]
    links <- map[SerializationKeys.links]
    locked <- map[SerializationKeys.locked]
    diffUrl <- map[SerializationKeys.diffUrl]
    patchUrl <- map[SerializationKeys.patchUrl]
    assignees <- map[SerializationKeys.assignees]
    milestone <- map[SerializationKeys.milestone]
    statusesUrl <- map[SerializationKeys.statusesUrl]
    id <- map[SerializationKeys.id]
    reviewCommentUrl <- map[SerializationKeys.reviewCommentUrl]
    base <- map[SerializationKeys.base]
    title <- map[SerializationKeys.title]
    commentsUrl <- map[SerializationKeys.commentsUrl]
    url <- map[SerializationKeys.url]
    issueUrl <- map[SerializationKeys.issueUrl]
    user <- map[SerializationKeys.user]
    updatedAt <- map[SerializationKeys.updatedAt]
    htmlUrl <- map[SerializationKeys.htmlUrl]
    mergeCommitSha <- map[SerializationKeys.mergeCommitSha]
    number <- map[SerializationKeys.number]
    authorAssociation <- map[SerializationKeys.authorAssociation]
    head <- map[SerializationKeys.head]
    commitsUrl <- map[SerializationKeys.commitsUrl]
    createdAt <- map[SerializationKeys.createdAt]
    reviewCommentsUrl <- map[SerializationKeys.reviewCommentsUrl]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = state { dictionary[SerializationKeys.state] = value }
    if let value = requestedReviewers { dictionary[SerializationKeys.requestedReviewers] = value }
    if let value = body { dictionary[SerializationKeys.body] = value }
    if let value = links { dictionary[SerializationKeys.links] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.locked] = locked
    if let value = diffUrl { dictionary[SerializationKeys.diffUrl] = value }
    if let value = patchUrl { dictionary[SerializationKeys.patchUrl] = value }
    if let value = assignees { dictionary[SerializationKeys.assignees] = value }
    if let value = milestone { dictionary[SerializationKeys.milestone] = value.dictionaryRepresentation() }
    if let value = statusesUrl { dictionary[SerializationKeys.statusesUrl] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = reviewCommentUrl { dictionary[SerializationKeys.reviewCommentUrl] = value }
    if let value = base { dictionary[SerializationKeys.base] = value.dictionaryRepresentation() }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = commentsUrl { dictionary[SerializationKeys.commentsUrl] = value }
    if let value = url { dictionary[SerializationKeys.url] = value }
    if let value = issueUrl { dictionary[SerializationKeys.issueUrl] = value }
    if let value = user { dictionary[SerializationKeys.user] = value.dictionaryRepresentation() }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = htmlUrl { dictionary[SerializationKeys.htmlUrl] = value }
    if let value = mergeCommitSha { dictionary[SerializationKeys.mergeCommitSha] = value }
    if let value = number { dictionary[SerializationKeys.number] = value }
    if let value = authorAssociation { dictionary[SerializationKeys.authorAssociation] = value }
    if let value = head { dictionary[SerializationKeys.head] = value.dictionaryRepresentation() }
    if let value = commitsUrl { dictionary[SerializationKeys.commitsUrl] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = reviewCommentsUrl { dictionary[SerializationKeys.reviewCommentsUrl] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.state = aDecoder.decodeObject(forKey: SerializationKeys.state) as? String
    self.requestedReviewers = aDecoder.decodeObject(forKey: SerializationKeys.requestedReviewers) as? [Any]
    self.body = aDecoder.decodeObject(forKey: SerializationKeys.body) as? String
    self.links = aDecoder.decodeObject(forKey: SerializationKeys.links) as? RequestsLinks
    self.locked = aDecoder.decodeBool(forKey: SerializationKeys.locked)
    self.diffUrl = aDecoder.decodeObject(forKey: SerializationKeys.diffUrl) as? String
    self.patchUrl = aDecoder.decodeObject(forKey: SerializationKeys.patchUrl) as? String
    self.assignees = aDecoder.decodeObject(forKey: SerializationKeys.assignees) as? [Any]
    self.milestone = aDecoder.decodeObject(forKey: SerializationKeys.milestone) as? RequestsMilestone
    self.statusesUrl = aDecoder.decodeObject(forKey: SerializationKeys.statusesUrl) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.reviewCommentUrl = aDecoder.decodeObject(forKey: SerializationKeys.reviewCommentUrl) as? String
    self.base = aDecoder.decodeObject(forKey: SerializationKeys.base) as? RequestsBase
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.commentsUrl = aDecoder.decodeObject(forKey: SerializationKeys.commentsUrl) as? String
    self.url = aDecoder.decodeObject(forKey: SerializationKeys.url) as? String
    self.issueUrl = aDecoder.decodeObject(forKey: SerializationKeys.issueUrl) as? String
    self.user = aDecoder.decodeObject(forKey: SerializationKeys.user) as? RequestsUser
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.htmlUrl = aDecoder.decodeObject(forKey: SerializationKeys.htmlUrl) as? String
    self.mergeCommitSha = aDecoder.decodeObject(forKey: SerializationKeys.mergeCommitSha) as? String
    self.number = aDecoder.decodeObject(forKey: SerializationKeys.number) as? Int
    self.authorAssociation = aDecoder.decodeObject(forKey: SerializationKeys.authorAssociation) as? String
    self.head = aDecoder.decodeObject(forKey: SerializationKeys.head) as? RequestsHead
    self.commitsUrl = aDecoder.decodeObject(forKey: SerializationKeys.commitsUrl) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.reviewCommentsUrl = aDecoder.decodeObject(forKey: SerializationKeys.reviewCommentsUrl) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(state, forKey: SerializationKeys.state)
    aCoder.encode(requestedReviewers, forKey: SerializationKeys.requestedReviewers)
    aCoder.encode(body, forKey: SerializationKeys.body)
    aCoder.encode(links, forKey: SerializationKeys.links)
    aCoder.encode(locked, forKey: SerializationKeys.locked)
    aCoder.encode(diffUrl, forKey: SerializationKeys.diffUrl)
    aCoder.encode(patchUrl, forKey: SerializationKeys.patchUrl)
    aCoder.encode(assignees, forKey: SerializationKeys.assignees)
    aCoder.encode(milestone, forKey: SerializationKeys.milestone)
    aCoder.encode(statusesUrl, forKey: SerializationKeys.statusesUrl)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(reviewCommentUrl, forKey: SerializationKeys.reviewCommentUrl)
    aCoder.encode(base, forKey: SerializationKeys.base)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(commentsUrl, forKey: SerializationKeys.commentsUrl)
    aCoder.encode(url, forKey: SerializationKeys.url)
    aCoder.encode(issueUrl, forKey: SerializationKeys.issueUrl)
    aCoder.encode(user, forKey: SerializationKeys.user)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(htmlUrl, forKey: SerializationKeys.htmlUrl)
    aCoder.encode(mergeCommitSha, forKey: SerializationKeys.mergeCommitSha)
    aCoder.encode(number, forKey: SerializationKeys.number)
    aCoder.encode(authorAssociation, forKey: SerializationKeys.authorAssociation)
    aCoder.encode(head, forKey: SerializationKeys.head)
    aCoder.encode(commitsUrl, forKey: SerializationKeys.commitsUrl)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(reviewCommentsUrl, forKey: SerializationKeys.reviewCommentsUrl)
  }

}
