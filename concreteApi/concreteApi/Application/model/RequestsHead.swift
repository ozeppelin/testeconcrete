//
//  RequestsHead.swift
//
//  Created by Anderson Silva on 08/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class RequestsHead: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let label = "label"
    static let user = "user"
    static let repo = "repo"
    static let ref = "ref"
    static let sha = "sha"
  }

  // MARK: Properties
  public var label: String?
  public var user: RequestsUser?
  public var repo: RequestsRepo?
  public var ref: String?
  public var sha: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    label <- map[SerializationKeys.label]
    user <- map[SerializationKeys.user]
    repo <- map[SerializationKeys.repo]
    ref <- map[SerializationKeys.ref]
    sha <- map[SerializationKeys.sha]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = label { dictionary[SerializationKeys.label] = value }
    if let value = user { dictionary[SerializationKeys.user] = value.dictionaryRepresentation() }
    if let value = repo { dictionary[SerializationKeys.repo] = value.dictionaryRepresentation() }
    if let value = ref { dictionary[SerializationKeys.ref] = value }
    if let value = sha { dictionary[SerializationKeys.sha] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.label = aDecoder.decodeObject(forKey: SerializationKeys.label) as? String
    self.user = aDecoder.decodeObject(forKey: SerializationKeys.user) as? RequestsUser
    self.repo = aDecoder.decodeObject(forKey: SerializationKeys.repo) as? RequestsRepo
    self.ref = aDecoder.decodeObject(forKey: SerializationKeys.ref) as? String
    self.sha = aDecoder.decodeObject(forKey: SerializationKeys.sha) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(label, forKey: SerializationKeys.label)
    aCoder.encode(user, forKey: SerializationKeys.user)
    aCoder.encode(repo, forKey: SerializationKeys.repo)
    aCoder.encode(ref, forKey: SerializationKeys.ref)
    aCoder.encode(sha, forKey: SerializationKeys.sha)
  }

}
