//
//  Extensions.swift
//  concreteApi
//
//  Created by Anderson Silva on 07/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
}

extension String {
    
    var formataData : String {
        
        let dtFormatter = DateFormatter()
        dtFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dtFormatter.locale = Locale(identifier: "pt_BR")
        let dt = dtFormatter.date(from: self)
        dtFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        return dtFormatter.string(from: dt!)
        
    }
    
}

extension UIColor {
    
    static func colorGreen() -> UIColor {
        return UIColor(red: 39/255.0, green: 134/255.0, blue: 93/255.0, alpha: 1.0)
    }
    
    static func colorRed() -> UIColor {
        return UIColor(red: 244/255.0, green: 67/255.0, blue: 54/255.0, alpha: 1.0)
    }
    
    static func colorGrey() -> UIColor {
        return UIColor(red: 241/255.0, green: 241/255.0, blue: 241/255.0, alpha: 1.0)
    }
    
    static func colorBorderGrey() -> UIColor {
        return UIColor(red: 216/255.0, green: 216/255.0, blue: 216/255.0, alpha: 1.0)
    }
    
    static func colorBorderButtonGrey() -> UIColor {
        return UIColor(red: 114/255.0, green: 117/255.0, blue: 124/255.0, alpha: 1.0)
    }
    
    static func colorBorderUITextField() -> UIColor {
        return UIColor(red: 190/255.0, green: 190/255.0, blue: 190/255.0, alpha: 1.0)
    }
    
    static func colorPedidoRealizado() -> UIColor {
        return UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
    }
    
    static func colorAguardando() -> UIColor {
        return UIColor(red: 16/255.0, green: 132/255.0, blue: 198/255.0, alpha: 1.0)
    }
    
    static func colorCancelado() -> UIColor {
        return UIColor(red: 114/255.0, green: 117/255.0, blue: 124/255.0, alpha: 1.0)
    }
    
    static func colorEntregue() -> UIColor {
        return UIColor(red: 50/255.0, green: 176/255.0, blue: 97/255.0, alpha: 1.0)
    }
    
    static func colorLogradouro() -> UIColor {
        return UIColor(red: 51/255.0, green: 56/255.0, blue: 65/255.0, alpha: 1.0)
    }
    
    static func colorMunicipio() -> UIColor {
        return UIColor(red: 142/255.0, green: 142/255.0, blue: 142/255.0, alpha: 1.0)
    }
    
    static func colorRedPage() -> UIColor {
        return UIColor(red: 255/255.0, green: 179/255.0, blue: 188/255.0, alpha: 1.0)
    }
    
}

extension UIButton {
    
    func addBlurEffect(style:UIBlurEffectStyle) {
        
        let effect = UIBlurEffect(style: style)
        let blur = UIVisualEffectView(effect: effect)
        blur.isUserInteractionEnabled = false
        self.insertSubview(blur, at: 0)
        
    }
    
    func addBorderAndColor(color:UIColor, border:CGFloat) {
        
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = border
        
    }
}

extension UITextField {
    func addBlur(style:UIBlurEffectStyle, view:UIView) {
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            let effect = UIBlurEffect(style: style)
            let blurEffectView = UIVisualEffectView(effect: effect)
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.backgroundColor = UIColor.clear
            
            self.background = view.imageWithView()
        }
    }
    
    func checkEmail() -> Bool {
        let valor = self.text!
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: valor, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, valor.count)) != nil
        } catch {
            return false
        }
    }
    
    func unmask() -> String! {
        var textOfField = self.text!
        textOfField = textOfField.replacingOccurrences(of: ".", with: "")
        textOfField = textOfField.replacingOccurrences(of: "-", with: "")
        textOfField = textOfField.replacingOccurrences(of: "(", with: "")
        textOfField = textOfField.replacingOccurrences(of: ")", with: "")
        return textOfField
    }
    
    func checkUF() -> Bool {
        
        let textUF = self.text!
        let ufUpper = textUF.uppercased()
        let ufArray = ["AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RO", "RS", "RR", "SC", "SE", "SP", "TO"]
        
        let value = ufArray.contains(ufUpper)
        return value
        
    }
}

extension UITableView {
    func hideEmptyLines() {
        self.tableFooterView = UIView()
    }
}

extension UIView {
    func imageWithView() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func clearHierarchyBackground() {
        self.backgroundColor = .clear
        if self.superview != nil {
            superview?.clearHierarchyBackground()
        }
    }
}

extension UIImageView{
    
    func asCircle(){
        self.layer.cornerRadius = self.frame.width / 2;
        self.layer.masksToBounds = true
    }
    
    func asCircleColor(color:UIColor) {
        self.layer.cornerRadius = self.frame.width / 2;
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color.cgColor
        self.layer.masksToBounds = true
    }
    
}

extension Int {
    
    func convertToDateiOS9() -> String {
        
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(self)/1000)  //UTC time
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local //Edit
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        return strDateSelect
        
    }
    
    func convertToDate() -> String {
        
        //Para recuperar os milisegundos dividir por 1000.
        if #available(iOS 10.0, *) {
            
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(self)/1000)  //UTC time
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local //Edit
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            
            let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
            return strDateSelect
            
        }else{
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(self))  //UTC time
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local //Edit
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            
            let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
            return strDateSelect
        }
    }
    
    var second: DateComponents {
        var components = DateComponents()
        components.second = self;
        return components
    }
    
    var seconds: DateComponents {
        return self.second
    }
    
    var minute: DateComponents {
        var components = DateComponents()
        components.minute = self;
        return components
    }
    
    var minutes: DateComponents {
        return self.minute
    }
    
    var hour: DateComponents {
        var components = DateComponents()
        components.hour = self;
        return components
    }
    
    var hours: DateComponents {
        return self.hour
    }
    
    var day: DateComponents {
        var components = DateComponents()
        components.day = self;
        return components
    }
    
    var days: DateComponents {
        return self.day
    }
    
    var week: DateComponents {
        var components = DateComponents()
        components.weekOfYear = self;
        return components
    }
    
    var weeks: DateComponents {
        return self.week
    }
    
    var month: DateComponents {
        var components = DateComponents()
        components.month = self;
        return components
    }
    
    var months: DateComponents {
        return self.month
    }
    
    var year: DateComponents {
        var components = DateComponents()
        components.year = self;
        return components
    }
    
    var years: DateComponents {
        return self.year
    }
    
}
extension DateComponents {
    
    var fromNow: Date {
        return Calendar.current.date(byAdding: self,
                                     to: Date())!
    }
    
    var ago: Date {
        let value = self
        return Calendar.current.date(byAdding: value,
                                     to: Date())!
    }
    
}

extension Bundle {
    
    var appName: String {
        return self.infoDictionary?["CFBundleName"] as! String
    }
    
    var releaseVersionNumber: String {
        return self.infoDictionary?["CFBundleShortVersionString"] as! String
    }
    
    var buildVersionNumber: String {
        return self.infoDictionary?["CFBundleVersion"] as! String
    }
    
}

