//
//  Constants.swift
//  concreteApi
//
//  Created by Anderson Silva on 07/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import Foundation

struct Constants {
    static let URL_ITEM = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="
    static let URL_REQUEST = "https://api.github.com/repos/"
}
