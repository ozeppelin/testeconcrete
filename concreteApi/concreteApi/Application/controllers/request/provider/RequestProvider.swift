//
//  RequestProvider.swift
//  concreteApi
//
//  Created by Anderson Silva on 08/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class RequestProvider : NSObject {
    
    func getRequests(fullName:String, completion:@escaping(_ resultado:[RequestsBaseClass]?, _ error:Error?) -> Void) -> Void {
        
        let url = "\(Constants.URL_REQUEST)\(fullName)/pulls"
        
        request(url).responseArray { (response:DataResponse<[RequestsBaseClass]>) in
            guard let resultado = response.result.value else {
                completion(nil, response.result.error)
                return
            }
            completion(resultado, nil)
        }
        
    }
    
}
