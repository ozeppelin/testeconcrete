//
//  RequestPullViewController.swift
//  concreteApi
//
//  Created by Anderson Silva on 08/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit

class RequestPullViewController: BaseViewController {

    @IBOutlet weak var tbView: UITableView!
    
    let manager = RequestManager()
    
    var objRequest:[RequestsBaseClass]?
    var objItens:ItemItems?
    var total = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addBackButton()
        manager.baseInterface = self
        manager.requestInterface = self
        
        self.title = self.objItens?.name
        self.tbView.delegate = self
        self.tbView.dataSource = self
        
        manager.getRequestPull(itens: self.objItens)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadXib() {
        
        let itensXib = UINib(nibName: "RequestPullTableViewCell", bundle: nil)
        self.tbView.register(itensXib, forCellReuseIdentifier: "cellRequestPull")
        
        self.tbView.tableFooterView = UIView()
        self.view.addSubview(self.tbView)
        self.tbView.reloadData()
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RequestPullViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "cellRequestPull", for: indexPath) as! RequestPullTableViewCell
        cell.baseInterface = self
        cell.setup(request: self.objRequest?[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.manager.openUrlSafari(url: self.objRequest![indexPath.row].htmlUrl!)
    }
    
}

extension RequestPullViewController : RequestInterface {
    
    func updateRequest(request: [RequestsBaseClass]) {
        self.objRequest = request
        self.total = request.count
        loadXib()
    }
    
}
