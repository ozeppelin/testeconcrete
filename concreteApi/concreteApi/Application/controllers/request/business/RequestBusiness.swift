//
//  RequestBusiness.swift
//  concreteApi
//
//  Created by Anderson Silva on 08/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import Foundation

class RequestBusiness : NSObject {
    
    private lazy var provider:RequestProvider = {
        return RequestProvider.init()
    }()
    
    func getRequests(fullName:String, completion:@escaping(_ resultado:[RequestsBaseClass]?, _ error:Error?) -> Void) -> Void {
        
        provider.getRequests(fullName: fullName) { (resultado, error) in
            guard resultado != nil else {
                completion(nil, error)
                return
            }
            
            completion(resultado, nil)
        }
        
    }
    
}



