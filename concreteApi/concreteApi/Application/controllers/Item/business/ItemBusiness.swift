//
//  ItemBusiness.swift
//  concreteApi
//
//  Created by Anderson Silva on 07/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import Foundation

class ItemBusiness : NSObject {
    
    private lazy var provider:ItemProvider = {
        return ItemProvider.init()
    }()
    
    func getItens(page:Int,completion:@escaping(_ itens:ItemBaseClass?, _ error:Error?) -> Void) -> Void {
        
        provider.getItens(page: page) { (item, error) in
            guard item != nil else {
                completion(nil, error)
                return
            }
            completion(item, nil)
        }

    }
    
}
