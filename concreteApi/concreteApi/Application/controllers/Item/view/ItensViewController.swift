//
//  ItensViewController.swift
//  concreteApi
//
//  Created by Anderson Silva on 07/11/2017.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit

class ItensViewController: BaseViewController {

    @IBOutlet weak var tbView: UITableView!
    
    var totalCount = 0
    let manager = ItemManager()
    var itens = [ItemItems]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "GitHub JavaPop"
        
        manager.baseInterface = self
        manager.itemInterface = self
        tbView.delegate = self
        tbView.dataSource = self
        
        manager.getItens()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadXib() {
        
        let itensXib = UINib(nibName: "ItensTableViewCell", bundle: nil)
        self.tbView.register(itensXib, forCellReuseIdentifier: "itensCell")
        
        self.tbView.tableFooterView = UIView()
        self.view.addSubview(self.tbView)
        self.tbView.reloadData()
        
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sgRequestPull" {
            let vc:RequestPullViewController = segue.destination as! RequestPullViewController
            vc.objItens = sender as? ItemItems
        }
     
     }
 

}
extension ItensViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.itens.count)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if ((self.itens.count < self.totalCount) && (self.itens.count-1 == indexPath.row)){
            manager.getItens()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "itensCell", for: indexPath) as! ItensTableViewCell
        cell.baseInterface = self
        cell.setup(item: self.itens[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showNextVC(identifier: "sgRequestPull", self.itens[indexPath.row])
    }
}

extension ItensViewController : ItemInterface {
    func updateItem(totalCount:  Int, items: [ItemItems]) {
        self.itens = items
        self.totalCount = totalCount
        self.loadXib()
    }
}
